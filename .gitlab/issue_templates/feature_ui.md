Le but de ce template est de décrire les étapes pour la réalisation d'une feature UI

## Le besoin

### Objectif

_(description synthétique de l'objectif, préciser éventuellement ce qui est hors scope)_

### Hors scope

### Résultats attendus

_(livrables et comment on teste)_

## Les spécifications:

### Maquettes

_(lien vers les maquettes + infos complémentaires)_

- [ ] Design Desktop
- [ ] Design Mobile

### Contenu et méthodologie

_(liens vers les docs de spec et de description des contenus, infos complémentaires si besoin)_

## Solution technique

_(analyse de faisabilité, orientations/justification de la solution, décomposition en sous composants pour l'UI. Cette partie doit permettre de réaliser le découpage en issues de dév)_

### Décomposition en composants:

### Décomposition en issues de dev:

/label ~"Type:Feature UI"
