
# Se positionner dans le repertoire parent de tous les repos => il faudrait un CRATER_HOME
cd ../..

echo "Status sur crater-data"
cd crater-data
git fetch
git status

echo "Status sur crater-data-resultats"
cd ../crater-data-resultats
git fetch
git status

echo "Fetch sur ui"
cd ../ui
git fetch
git status
