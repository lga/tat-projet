# LGA - Gestion Projets

Ce projet contient des éléments communs à tous les repos du groupe :

* des [scripts de livraison](scripts_livraison) pour les mises en prod
* et des [boards](https://framagit.org/lga/tat-projet/-/boards) des principales évolutions envisagées sur la feuille de route du projet
* un [wiki](https://framagit.org/lga/projet/-/wikis/home) avec différents guides
